#!/bin/bash

Help()
{
   echo "Kubernetes deployment generator"
   echo
   echo "Syntax: scriptTemplate \$Longitude \$Latitude \$Hostname"
   echo
}

while getopts ":h" option; do
   case $option in
      h)
         Help
         exit;;
     \?)
         echo "Error: Invalid option"
         exit;;
   esac
done

if [ $# -eq 3 ]
then
  export longitude=$1
  export latitude=$2
  export hostname=$3
  file="deployment_${hostname}.yml"
  rm -f $file temp.yml
  ( echo "cat <<EOF >$file";
    cat template_deployment.yml;
    echo "EOF";
  ) >temp.yml
  . temp.yml
else
  Help
fi

