import os
import time
from datetime import datetime
import paho.mqtt.client as mqtt
from os import environ
import subprocess
import json
import re
from typing import NamedTuple
from influxdb_client import InfluxDBClient, Point, WritePrecision
from influxdb_client.client.write_api import SYNCHRONOUS

MQTT_REGEX = 'aci/([^/]+)/([^/]+)'

class SensorData(NamedTuple):
    topicChannel : str
    type: str
    rawValue: int
    raspberryID : str


class InfluxCli():
    def __init__(self, uri):
        self.org = environ['INFLUX_ORG']
        self.bucket = environ['INFLUX_BUCKET']
        self.username = environ['INFLUX_USER']
        self.password = environ['INFLUX_PASS']
        self.start_db()
        self.setup_db()
        self.influxdb_client = InfluxDBClient(url=uri, token=self.token)
        self.write_api = self.influxdb_client.write_api(write_options=SYNCHRONOUS)


    def send_sensor_data_to_influxdb(self, client ,sensor_data):
        print(type(sensor_data.rawValue))
        point = Point("mem").tag("raspberryID", sensor_data.raspberryID).field(sensor_data.type, int(sensor_data.rawValue)).time(datetime.utcnow(), WritePrecision.NS)
        print(type(sensor_data.rawValue))
        print(point)
        self.write_api.write(self.bucket, self.org, point)

    def parse_mqtt_message(self, topic, payload, rpiID ):
        """champ dans mqtt clientID"""
        match = re.match(MQTT_REGEX, topic)
        if match:
            topicChanel = match.group(1)
            type = match.group(2)
            return SensorData(topicChanel, type, payload,rpiID  )

        else:
            return None

    def add_data(self, client, userdata, msg):
        """The callback for when a PUBLISH message is received from the server."""
        print(msg.topic + ' ' + str(msg.payload))
        sensor_data = self.parse_mqtt_message(msg.topic, msg.payload.decode('utf-8') , client) #._client_id=champs_vide
        if sensor_data is not None:
            self.send_sensor_data_to_influxdb(influxdb_client,sensor_data)

    def start_db(self):
        subprocess.Popen("nohup /entrypoint.sh >/dev/null 2>&1 &", shell=True)
        # Give some time to the database to start
        time.sleep(5)

    def setup_db(self):
        # Setup the database at the first launch with the provided info
        subprocess.Popen(f"influx setup --org {self.org} --bucket {self.bucket} --username {self.username} --password {self.password} --force", shell=True)
        time.sleep(3)
        # Retrieve the database configuration for the created user
        json_config = subprocess.run(["influx", "auth", "list", "--json"], capture_output=True)
        time.sleep(3)
        # Transform the output of the previous command in json format
        dump_json = json.loads(json_config.stdout)
        # Parse the json and extract the token
        self.token = dump_json[0]['token']


class MQTTClient():

    def __init__(self, urlName, port):
        self.client = mqtt.Client()
        self.client.connect(urlName, port)

    def subscribe(self, topic):
        self.client.subscribe(topic)
        self.client.loop_forever()

if __name__ == '__main__':
    c = MQTTClient(environ['MQTT_ADDRESS'], int(environ['MQTT_PORT']))
    influxdb_client = InfluxCli("http://localhost:8086")
    c.client.on_message = influxdb_client.add_data
    c.subscribe('aci/#')
