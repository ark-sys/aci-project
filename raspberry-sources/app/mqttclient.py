import paho.mqtt.client as mqtt
class MQTTClient():

    # Initialize the mqtt client and connect to the specified broker on url and port
    def __init__(self, id, urlName, port=1883):
        self.client = mqtt.Client(client_id=id)
        self.client.connect(urlName, port)

    # Publish data on the specified topic
    def publish(self, topic, data):
        self.client.publish(topic, data, qos=0)
