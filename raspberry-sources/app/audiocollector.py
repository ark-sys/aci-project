import pyaudio  # Soundcard audio I/O access library
# Setup audio capture mode
FORMAT = pyaudio.paInt16  # data type formate
CHANNELS = 1  # Adjust to your number of channels
RATE = 48000  # Sample Rate
CHUNK = 2048  # Block Size

class AudioCollector():
    # Initialize audio device
    def __init__(self):
        self.audio = pyaudio.PyAudio()
        self.startRecord()

    # Retrieve metadata regarding audio capture mode
    def getWAVdata(self):
        return CHANNELS, self.audio.get_sample_size(FORMAT), RATE, CHUNK

    # Open the default recording device available in the system with the specified capture mode
    def startRecord(self):
        self.stream = self.audio.open(format=FORMAT, channels=CHANNELS, rate=RATE, input=True, output=False,frames_per_buffer=CHUNK)

    # Retrieve chunks of audio from the opened device
    def getAudio(self):
        return self.stream.read(CHUNK, exception_on_overflow=False)

    # Close the audio stream and destroy the device
    def stopRecord(self):
        self.stream.stop_stream()
        self.stream.close()
        self.audio.terminate()
