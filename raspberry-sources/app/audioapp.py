import os

import audiocollector
import mqttclient

if __name__ == '__main__':
    # Create a device object that will interface with the microphone
    device = audiocollector.AudioCollector()

    # Create an mqtt connection on the specified ip and port broker
    connection = mqttclient.MQTTClient(os.environ['PI_HOSTNAME'], os.environ['IP_BROKER'],
                                       int(os.environ['PORT_BROKER']))

    # Build topic name for the current RaspberryPi
    topic = os.environ['AUDIO_TOPIC']

    # Start recording data from the microphone
    device.startRecord()

    print("Recording...")
    # Start connection in loop mode so that the client can recover from errors
    connection.client.loop_start()
    while True:

        # Publish chunks of audio on the specified topic
        connection.publish(topic ,device.getAudio())

