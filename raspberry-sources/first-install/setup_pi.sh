#!/usr/bin/env sh

# Check that the script is being run as sudop
if [ `whoami` != root ]; then
    echo "Please run this script as root or using sudo"
    exit
fi

# Update repo
apt-get update && apt-get upgrade

# Update docker configuration file and restart the service
systemctl stop docker.service
cp confs/docker.service /lib/systemd/system/docker.service
systemctl daemon-reload
systemctl start docker.service

# Setup kubernetes configuration files
cp confs/k8s.conf /etc/sysctl.d/k8s.conf
cp confs/modules.conf /etc/modules-load.d/modules.conf
sysctl --system

# Disable swap (kubernetes doesn't like it)
swapoff -a

# Setup a hostname for the pi
echo "enter Pi hostname: "
read hname
hostnamectl set -hostname "$hname"

# Install kubeadm, kubectl, kubelet
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add
apt-add-repository "deb http://apt.kubernetes.io/ kubernetes-xenial main"
apt-get update && apt-get upgrade
apt install -y kubeadm kubectl kubelet

# Setup a VPN connection
apt install openvpn
cp certs/ /etc/openvpn/
cp confs/vpn.service /lib/systemd/system/vpn.service # Think of a new name for the service like acivpn or something
systemctl start vpn.service

#Â Join kubernetes master
#A voir si on reboot la pi avant ou apres le join
kubeadm join IP_API_KUBE:6443 --token TOKEN --discovery-token-ca-cert-hash sha256:856fccc8087d580614af199cc658c24d770d86861c2ce25c1a433ab141c5e5ca

# Reboot to apply modification
reboot now